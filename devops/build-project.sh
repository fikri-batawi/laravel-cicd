#!/bin/bash

composer install --no-interaction
php artisan config:clear

ln -f -s .env.pipelines .env

php artisan migrate --no-interaction
php artisan key:generate