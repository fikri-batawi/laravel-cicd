#!/bin/bash

apt -qy update
apt -qy install curl git zip unzip

docker-php-ext-install pdo_mysql ctype bcmath zip

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer